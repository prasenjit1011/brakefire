<?php

Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'Api\V1\Admin', 'middleware' => ['auth:api']], function () {

    // Categories
    Route::apiResource('categories', 'CategoryApiController');
    // Services
    Route::post('services/media', 'ServicesApiController@storeMedia')->name('services.storeMedia');
    Route::apiResource('services', 'ServicesApiController');

    // Certificates
    Route::post('certificates/media', 'CertificateApiController@storeMedia')->name('certificates.storeMedia');
    Route::apiResource('certificates', 'CertificateApiController');

    // Downloads
    Route::post('downloads/media', 'DownloadsApiController@storeMedia')->name('downloads.storeMedia');
    Route::apiResource('downloads', 'DownloadsApiController');

    // Website Configs
    Route::apiResource('website-configs', 'WebsiteConfigApiController');
    // Content Management Systems
    Route::post('content-management-systems/media', 'ContentManagementSystemApiController@storeMedia')->name('content-management-systems.storeMedia');
    Route::apiResource('content-management-systems', 'ContentManagementSystemApiController');

    // Permissions
    Route::apiResource('permissions', 'PermissionsApiController');

    // Roles
    Route::apiResource('roles', 'RolesApiController');

    // Users
    Route::apiResource('users', 'UsersApiController');

    // Fire Installations
    Route::post('fire-installations/media', 'FireInstallationApiController@storeMedia')->name('fire-installations.storeMedia');
    Route::apiResource('fire-installations', 'FireInstallationApiController');

    // Fire Maintenances
    Route::post('fire-maintenances/media', 'FireMaintenanceApiController@storeMedia')->name('fire-maintenances.storeMedia');
    Route::apiResource('fire-maintenances', 'FireMaintenanceApiController');

    // Fire Supplies
    Route::post('fire-supplies/media', 'FireSupplyApiController@storeMedia')->name('fire-supplies.storeMedia');
    Route::apiResource('fire-supplies', 'FireSupplyApiController');

    // Enquiries
    Route::post('enquiries/media', 'EnquiryApiController@storeMedia')->name('enquiries.storeMedia');
    Route::apiResource('enquiries', 'EnquiryApiController');

    // Bookings
    Route::apiResource('bookings', 'BookingApiController');

    // Visits
    Route::post('visits/media', 'VisitApiController@storeMedia')->name('visits.storeMedia');
    Route::apiResource('visits', 'VisitApiController');

    // Complains
    Route::post('complains/media', 'ComplainApiController@storeMedia')->name('complains.storeMedia');
    Route::apiResource('complains', 'ComplainApiController');

    // Employees
    Route::post('employees/media', 'EmployeeApiController@storeMedia')->name('employees.storeMedia');
    Route::apiResource('employees', 'EmployeeApiController');

    // Customers
    Route::apiResource('customers', 'CustomerApiController');

    // Cctv Installations
    Route::post('cctv-installations/media', 'CctvInstallationApiController@storeMedia')->name('cctv-installations.storeMedia');
    Route::apiResource('cctv-installations', 'CctvInstallationApiController');

    // Ac Installations
    Route::post('ac-installations/media', 'AcInstallationApiController@storeMedia')->name('ac-installations.storeMedia');
    Route::apiResource('ac-installations', 'AcInstallationApiController');

    // Telecom Installations
    Route::post('telecom-installations/media', 'TelecomInstallationApiController@storeMedia')->name('telecom-installations.storeMedia');
    Route::apiResource('telecom-installations', 'TelecomInstallationApiController');

    // Gas Installations
    Route::post('gas-installations/media', 'GasInstallationApiController@storeMedia')->name('gas-installations.storeMedia');
    Route::apiResource('gas-installations', 'GasInstallationApiController');

    // Network Installations
    Route::post('network-installations/media', 'NetworkInstallationApiController@storeMedia')->name('network-installations.storeMedia');
    Route::apiResource('network-installations', 'NetworkInstallationApiController');

    // Electronics Installations
    Route::post('electronics-installations/media', 'ElectronicsInstallationApiController@storeMedia')->name('electronics-installations.storeMedia');
    Route::apiResource('electronics-installations', 'ElectronicsInstallationApiController');

    // G Maintinance Installations
    Route::post('g-maintinance-installations/media', 'GMaintinanceInstallationApiController@storeMedia')->name('g-maintinance-installations.storeMedia');
    Route::apiResource('g-maintinance-installations', 'GMaintinanceInstallationApiController');

    // Sanatization Installations
    Route::post('sanatization-installations/media', 'SanatizationInstallationApiController@storeMedia')->name('sanatization-installations.storeMedia');
    Route::apiResource('sanatization-installations', 'SanatizationInstallationApiController');

    // Cleaning Installations
    Route::post('cleaning-installations/media', 'CleaningInstallationApiController@storeMedia')->name('cleaning-installations.storeMedia');
    Route::apiResource('cleaning-installations', 'CleaningInstallationApiController');
});
